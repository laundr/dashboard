var Laundr = angular.module('Laundr', ['Laundr.Factories']);
var root = 'https://laundr.herokuapp.com';

Laundr
.controller('BaseCtrl', [
  '$scope', '$http', 'api', 'Base64', 'behaviour',
  function($scope, $http, $api, Base64, details) {
    $scope['new' + details.model] = function() {
      $scope[details.internal] = {};
    }

    $scope['save' + details.model] = function(data) {
      $api[details.model].save(
        data,
        function(val, hdr) {
          console.log('saved ' + details.model);
          $(details.model + 'Modal').modal('hide');
          $scope.update();
        },
        function(res) {
          console.log('problem saving ' + details.model);
        }
      );
    }

    if(typeof($scope[details.resources]) == 'undefined') {
      var criteria = {};
      var resources = $api[details.model].query(criteria, function() {
        $scope[details.resources] = resources.map(function(resources) {
          var res = details.format(resource);
          res.id = resource.id;
          return res;
        });
      });
    }
  }
])
.controller('UserCtrl', ['$scope', '$http', 'api', 'Base64', function($scope, $http, api, Base64) {
  $scope.credentials = {
  }
  $scope.update = function() {
    console.log('updating');
    var users = api.User.query(function() {
      $scope.users = users;
    });
  }

  $scope.registerUser = function (credentials) {
    console.log('admin is ' + credentials.admin);
    var user = {
      name: credentials.name,
      handle: credentials.handle,
      secret: credentials.secret,
      repeat: credentials.repeat,
      title: credentials.title,
      phonenumber: credentials.phonenumber,
      phone: credentials.phonenumber,
    };
    api.User.save(
      user, 
      function(val, hdr) {
        console.log('registered');
        $('#signupModal').modal('hide');
        $scope.update();
      }, 
      function(res) {
        console.log('problem');
        console.log(res);
      }
    );
  };
 
  $scope.updateUser = function (user) {
    // FIX: back-end works with phonenumber for registration and phone for update
    user.phone = user.phonenumber;
    console.log('updating user w/ id ' + user.id);
    api.User.update({ id: user.id }, user);  
  }

  $scope.removeUser = function(user) {
    user.$delete(function() {
      console.log('user removed');
    });
  } 

  $scope.setCredentials = function(credentials) {
    console.log('Setting creds to ' + credentials.handle + ' ' + credentials.secret);
    var encoded = Base64.encode(credentials.handle + ':' + credentials.secret);
    $http.defaults.headers.common.Authorization = 'Basic ' + encoded;
    console.log($http.defaults.headers.common.Authorization);
    $scope.update('Basic ');
    $('#loginModal').modal('hide');
  }

  $scope.clearCredentials = function() {
    document.execCommand("ClearAuthenticationCache");
    $http.defaults.headers.common.Authorization = 'Basic ';
  }
  
  $scope.logOut = function() {
    $scope.clearCredentials();
    $http.defaults.headers.common.Authorization = null;
  }

  $scope.debug = function() {
    console.log('User is ' + $scope.credentials.handle + ' w/ ' + $scope.credentials.secret);
    console.log('creds set is ' + $scope.flags.creds_set);
    console.log(Object.keys($scope.flags)); //.flags.creds_set);
  }

  $scope.$watch('credentials', function(newValue, oldValue) {
    if(newValue == {}) {
      console.log("set credentials");
    } else {
      console.log("clear credentials");
    }
  });

  $scope.update();
}]);

Laundr.controller('GarmentCtrl', ['$scope', '$http', 'api', 'Base64', function($scope, $http, api, Base64) {
  $scope.update = function() {
    console.log('update garments');
    var garments = api.Garment.query(function() {
      $scope.garments = garments;
    });
  }

  $scope.createGarment = function (garment) {
    var garment = garment;
    api.Garment.save(
      garment,
      function(val, hdr) {
        console.log('garment saved');
        $('#garmentModal').modal('hide');
        $scope.update();
      },
      function(res) {
        console.log('garment creation problem');
        console.log(res);
      }
    );
  };

  $scope.updateGarment = function (garment) {
    console.log('updating garment w/ id ' + garment.id);
    api.Garment.update({ id: garment.id }, garment);  
  }

  $scope.removeGarment = function(garment) {
    garment.$delete(function() {
      console.log('garment removed');
    });
  }

  $scope.update();
}]);
  
Laundr.controller('CityCtrl', ['$scope', '$http', 'api', 'Base64', function($scope, $http, api, Base64) {
  $scope.update = function() {
    var cities = api.City.query(function() {
      $scope.cities = cities;
    });

    var orders = api.Order.query(function() {
      $scope.orders = orders;
    });
  }

  $scope.createCity = function (city) {
    var city = city;
    api.City.save(
      city,
      function(val, hdr) {
        console.log('created');
        $('#cityModal').modal('hide');
        $scope.update();
      },
      function(res) {
        console.log('city creation problem');
        console.log(res);
      }
    );
  };

  $scope.updateCity = function (city) {
    console.log('updating city w/ id ' + city.id);
    api.City.update({ id: city.id }, city);  
  }

  $scope.removeCity = function(city) {
    city.$delete(function() {
      console.log('city removed');
    });
  }

  $scope.update();
}]);
  
Laundr.controller('OrderCtrl', ['$scope', '$http', 'Base64', function($scope, $http, Base64) {
  $scope.orders = [];
  $scope.cities = [];
  
  $scope.formReset = function() {
    $scope.new = { items: [], delivery: {}, pickup: {} };
  }

  $scope.update = function() {
    $http.get('/orders').success(function(data){
      console.log('orders');
      $scope.orders = data.map(function(order){
        var order = order;
        order.pickup.time = {
          description: moment(order.pickup.time).calendar(),
          stamp: order.pickup.time
        }
        order.delivery.time = {
          description: moment(order.delivery.time).calendar(),
          stamp: order.delivery.time
        }
        console.log(order.pickup.time.description + " till " + order.delivery.time.description);
        return order;
      });
    });
  }

  $scope.addNewItem = function() {
    $scope.new.items.push({});
  }

  $scope.removeItem = function(idx) {
    $scope.new.items.splice(idx, 1)
  }

  $scope.placeOrder = function(details) {
    console.log(details);
    var order = {
      items: details.items.map(function(item){
        return {
          amount: item.amount,
          garment: item.type.id
        }
      }),
      delivery: {
        time: details.delivery.time,
        location: {
          city: details.delivery.city.id,
          postalcode: details.delivery.postalcode,
          street: details.delivery.street,
          number: details.delivery.number
        }
      },
      pickup: {
        time: details.pickup.time,
        location: {
          city: details.pickup.city.id,
          postalcode: details.pickup.postalcode,
          street: details.pickup.street,
          number: details.pickup.number
        }
      }
    }
    $http.post('/orders', order).success(function(data){
      console.log('saved order');
      $scope.formReset();
      $scope.update();
    });
  }

  $scope.formReset();
}]);
